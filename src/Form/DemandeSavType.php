<?php

namespace App\Form;

use App\Entity\DemandeSav;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class DemandeSavType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('email', EmailType::class, [
                'label' => 'Adresse email',
            ])
            ->add('createdAt', null, [
                'label' => "Date de la demande d'intervention ",
                'widget' => 'single_text',
            ])
            ->add('adresse', TextType::class, [
                'label' => 'Adresse',
            ])
            ->add('cp', TextType::class, [
                'label' => 'Code Postal',
            ])
            ->add('ville', TextType::class, [
                'label' => 'Ville',
            ])
            ->add('nom', TextType::class, [
                'label' => 'Votre nom',
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Votre prénom',
            ])
            ->add('telephone', null, [
                'label' => 'Téléphone',
            ])
            ->add('modele', ChoiceType::class, [
                'label' => 'Modèle de machine',
                'choices' => [
                    'A300' => 'A300',
                    'A400' => 'A400',
                    'A600' => 'A600',
                    'A800' => 'A800',
                    'A1000' => 'A1000',
                    'BIEPI' => 'BIEPI',
                    'Autre' => 'Autre',
                ],
            ])
            ->add('serialNumber', null, [
                'label' => 'Numéro de série',
                'attr' => ['placeholder' => 'Au dos de la machine ou sur le côté'],
            ])
            ->add('localisation', TextType::class, [
                'label' => "Lieu d'installation (RDC, 1er étage, ...)",
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Veuillez décrire la panne rencontrée',
            ])
            ->add('imageFile', VichFileType::class, ['required' => false, 'label' => 'Photo (recommandée)'])
            ->remove('imageName')
            ->remove('imageSize')
            ->remove('updatedAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DemandeSav::class,
        ]);
    }
}
