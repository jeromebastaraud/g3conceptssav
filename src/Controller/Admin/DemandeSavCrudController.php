<?php

namespace App\Controller\Admin;

use App\Entity\DemandeSav;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DemandeSavCrudController extends AbstractCrudController
{
    use Trait\DetailTrait;

    public static function getEntityFqcn(): string
    {
        return DemandeSav::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->remove(Crud::PAGE_DETAIL, Action::EDIT);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt', 'Date de création'),
            TextField::new('adresse'),
            NumberField::new('cp', 'Code Postale'),
            TextField::new('ville'),
            TextField::new('nom'),
            TextField::new('prenom'),
            NumberField::new('telephone', 'Téléphone'),
            TextField::new('modele', 'Modèle'),
            TextField::new('serialNumber', ' Numéros de Serie'),
            TextField::new('localisation'),
            TextField::new('description'),
            EmailField::new('email'),
            ImageField::new('imageName', 'Photos')->setBasePath('/images/photos'),
        ];
    }
}
