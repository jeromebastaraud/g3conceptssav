<?php

namespace App\Controller;

use App\Entity\DemandeSav;
use App\Form\DemandeSavType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(
        public EntityManagerInterface $em,
    ) {
    }

    #[Route('/', name: 'app_home')]
    public function index(Request $request, MailerInterface $mailer): Response
    {
        // Mise en place du formulaire pour le rendu coté front
        $demandeSav = new DemandeSav();
        $form = $this->createForm(DemandeSavType::class, $demandeSav);
        // hydratation du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            dd($data);
            $email = (new TemplatedEmail())
                ->from($data->getEmail())
                ->to('tyss@me.com')
                ->subject('vous avez reçu une nouvelle demande')
                ->html('emails/demandeSav.html.twig')
                ->context([
                    'demande' => $data,
                ]);
            $mailer->send($email);
            $this->em->persist($demandeSav);
            $this->em->flush();
            $this->addFlash('success', 'Vore message a été envoyé');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('home/index.html.twig', [
            'demandeSavForm' => $form->createView(),
        ]);
    }
}
