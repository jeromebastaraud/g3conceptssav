<?php

namespace App\Repository;

use App\Entity\DemandeSav;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DemandeSav>
 *
 * @method DemandeSav|null find($id, $lockMode = null, $lockVersion = null)
 * @method DemandeSav|null findOneBy(array $criteria, array $orderBy = null)
 * @method DemandeSav[]    findAll()
 * @method DemandeSav[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemandeSavRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DemandeSav::class);
    }

    //    /**
    //     * @return DemandeSav[] Returns an array of DemandeSav objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DemandeSav
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
