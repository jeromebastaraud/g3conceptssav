import './bootstrap.js';
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉')

import "@hotwired/stimulus";
import "@symfony/stimulus-bundle";
import "@hotwired/turbo";
import "tw-elements";
import "@tailwindcss/forms";

// Initialization for ES Users
import {
  Input,
  Ripple,
  Collapse,
  initTE,
} from "tw-elements";

initTE({ Input, Ripple, Collapse });